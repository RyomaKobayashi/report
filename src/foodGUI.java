import javax.swing.*;import javax.swing.event.AncestorListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodGUI {
    private JPanel root;
    private JButton Tempura;
    private JButton Udon;
    private JButton Ramen;
    private JButton Soba;
    private JButton Friedrice;
    private JButton Unaju;
    private JButton CheckOut;
    private JTextPane orderedItemsList;
    private JTextPane total;

    int sum=0;
    public foodGUI() {
        Tempura.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.jpg")
        ));

        Ramen.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.jpg")
        ));

        Udon.setIcon(new ImageIcon(
                this.getClass().getResource("Udon.jpg")
        ));

        Soba.setIcon(new ImageIcon(
                this.getClass().getResource("Soba.jpg")
        ));

        Friedrice.setIcon(new ImageIcon(
                this.getClass().getResource("Friedrice.jpg")
        ));

        Unaju.setIcon(new ImageIcon(
                this.getClass().getResource("Unagi.jpg")
        ));

        Tempura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",500);
            }
        });

        Ramen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",800);
            }
        });

        Udon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",550);
            }
        });

        Soba.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",900);
            }
        });

        Friedrice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried Rice",750);
            }
        });

        Unaju.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Unaju",2000);
            }
        });

        CheckOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();
            }
        });
    }

    void order(String food,int price){
                int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order "+ food + "?","Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    String currentText = orderedItemsList.getText();
                    orderedItemsList.setText(currentText+ food +" " +price + " yen\n" );
                    JOptionPane.showMessageDialog(null,"Order for "+ food +" received.");
                    sum = sum+price;
                    total.setText("Total          " + sum + " yen");
                };
    }
    void checkout(){
        int i=0;
        if(sum==0) {
            JOptionPane.showMessageDialog(null,"Nothing ordered.");
            i=1;
        }
        if(i==0) {
            int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to checkout?", "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == 0) {
                JOptionPane.showMessageDialog(null, "Thank you! Total price is " + sum + " yen.");
                sum = 0;
                orderedItemsList.setText("");
                total.setText("Total          0 yen");
            }
            ;
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodGUI");
        frame.setContentPane(new foodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
